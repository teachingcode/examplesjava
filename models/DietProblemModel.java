package models;

// Necessary imports of classes 
// which are not in our package
import ilog.concert.IloException;
import ilog.concert.IloLinearNumExpr;
import ilog.concert.IloNumVar;
import ilog.concert.IloRange;
import ilog.cplex.IloCplex;
import optimizationProblems.DietProblem;

/**
 * This class creates the template for objects representing
 * mathematical models for the Diet Problem.
 * An instance of this class represents the mathematical
 * model for a specific instance of the Diet Problem.
 * Remember: different instance - > different data.
 * @author lct495
 */
public class DietProblemModel {
    
    // Every class representing an 
    // optimization problem must have 
    // an IloCplex object. This is the
    // container of the mathematical 
    // programming elements.
    // Since we will not modify 
    // the variables holding the 
    // model (we modify the object but 
    // not the variable .. think about 
    // the difference) and the problem 
    // we can make them final.
    private final IloCplex model;
    // Stores the problem, the variables and the constraints
    // in order to access them in the methods of the class
    // DietProblemModel.
    private final DietProblem problem;
    private final IloNumVar[] x;
    private final IloRange[] constr;
    
    public DietProblemModel(DietProblem problem) throws IloException{
        // Creates the IloCplex object
        // and stores the problem in the
        // field variable. 
        model = new IloCplex();
        this.problem = problem;
        
        // Create the decision variables.
        // We have one decision variable for each food
        // thus we create an array of decision variables
        // with as many positions as there are foods.
        x = new IloNumVar[problem.getNFoods()];
        
        // Now the array is just an empty container for those
        // type of objects. Thus we need to populate it
        // with objects of type IloNumVar.
        for(int i = 1; i <= problem.getNFoods(); i++){
            x[i-1] = model.numVar(0, Double.POSITIVE_INFINITY,"x_"+i);
        }
        
        // Create and add the objective function.
        // First we create an empty linear expression.
        IloLinearNumExpr objective = model.linearNumExpr();
        
        // Then we add the terms of the expression.
        for(int i = 1; i <= problem.getNFoods(); i++){
            objective.addTerm(x[i-1], problem.getCost(i));
        }
        
        // Finally we tell the model to minimize that 
        // linear expression.
        model.addMinimize(objective);
        
        // Create and add the constraints.
        // First we create an array of IloRange objects
        constr = new IloRange[problem.getNNutrients()];
        // Now the array is empty. It is simply 
        // a sequence of containers for objects of
        // type IloRange. As many containers as there are
        // constraints (that is as many nutrients).
        
        // We have one constraint for each nutrient.
        // Thus for each nutrient we have a linear expression. 
        for(int j = 1; j <= problem.getNNutrients(); j++){
            IloLinearNumExpr lhs = model.linearNumExpr();
            // We add the terms to the linear expression
            for(int i = 1; i <= problem.getNFoods(); i++){
                lhs.addTerm(x[i-1], problem.getFoodsComposition(i, j));
            }
            // Finally we add the constraint to the model 
            // telling cplex that the linear expression 
            // must be greater than the requirement of the 
            // nutrient. The method addGe adds the constraint
            // to the model and returns the constraint (an
            // object of type IloRange) which we store in
            // our array constr. 
            constr[j-1] = model.addGe(lhs, problem.getNutrientsRequirement(j),"ConstrNutrient_"+j);
        }
    }
    /**
     * Solves the problem and prints the result.
     * @throws IloException 
     */
    public void solve() throws IloException{
        boolean has_feasible_solution = model.solve();
        if(has_feasible_solution){
            System.out.println("Optimal value "+model.getObjValue());
        }else{
            System.out.println("No feasible solution has been found");
        }
    }
    public void printSolution() throws IloException{
        System.out.println("Solution: ");
        for(int i = 1; i <= problem.getNFoods(); i++){
            // We use the method getValue to obtain the 
            // value of a given decision variable after the solver
            // has completed its solution process. 
            System.out.println("Buy "+model.getValue(x[i-1])+" of food "+problem.getFoodName(i));
        }
    }
    /**
     * Prints the model.
     */
    public void print(){
        // The method toString returns a human-readable 
        // summary of the model.
        System.out.println(model.toString());
    }
    /**
     * Prints the optimal dual variables associated with the constraints. 
     * @throws IloException 
     */
    public void printDuals() throws IloException{
        for(int j = 1; j <= problem.getNNutrients(); j++){
            System.out.println("Dual const "+j+" = "+model.getDual(constr[j-1]));
        }
    }
    
}
