
package mainPackage;

// Note that the classes which are not 
// in the package of the Main class must be imported
// in order for the Main class to seen them.
import bicycle.Bicycle;
// Assume that there are two classes in the bicycle package and that we wanted 
// to use both of them. We would need to import both of them separately.
// Alternatively we could use "import bicycle.*;" to import
// every class in the package. This option is however not advisable. 
import geometry.Point;
import geometry.Rectangle;
import ilog.concert.IloException;
import optimizationProblems.DietProblem;
import inputOutput.Reader;
import inputOutput.Writer;
import java.io.FileNotFoundException;
import models.DietProblemModel;




/**
 * The Main class contains the main method.
 * Inside the main method we do some tests with the other classes. 
 * @author lct495
 */
public class Main {
    public static void main(String[] args) throws FileNotFoundException, IloException{
        
        // We create a bicycle object
        Bicycle b1 = new Bicycle(3,4);
        // We create another bicycle object
        Bicycle b2 = new Bicycle(3,5);
        // More precisely we have created variables named
        // b1 and b2 which point to objects of type Bicycles.
        
        // PASSING BY VALUE AND BY REFERENCE.
        // A difference between primitive types (String, int, double, boolean, etc.)
        // and objects, is that primitive types are passed "by value",
        // while objects are passed "by reference".
        // Consider the following example:
        int a = 10;
        int b = a;
        // The "value" of a has been passed to b (not its address in the memory).
        // Now both a and b are equal to 10:
        System.out.println("a: "+a+ " b: "+b);
        // The fact that primitive types are passed by value implies that
        // upon changing b, I am only changing the value of b. That is, 
        // now b is a completely independent entity which holds the same value
        // as a. Changes on b do not have any effect on a. Try this:
        b++;
        System.out.println("a: "+a+ " b: "+b);
        // Now b = 11 and a is still 10.
        
        // On the other hand, objects are passed by reference. 
        // Consider b1 created above. Now I assign b1 to another variable, say b3
        Bicycle b3 = b1;
        // By doing this, I have not passed the value of b1 to b3. Rather, 
        // I have passed a reference to it, the address of its home in the memory.
        // In fact, consider the following example. Now I change b3:
        b3.changeGear(100);
        // I changed the gear of b3, while I did not do anything on b1.
        // Let us print some information:
        b1.print();
        b3.print();
        // You can see that both b1 and b3 print that the gear is 100.
        // In fact, the variables b1 and b3 are indeed pointing to the same
        // individual object and not to two different objects.
        // By acting on b1 or b3, changes are made on the same object. 
        // This is an important issue to keep in mind in order to avoid
        // unplanned results. This is particularly true when passing an 
        // object to a method: you are not passing the value or a copy of the
        // object, but rather the address to the object itself. Thus, if the 
        // method is supposed to modify the object received, the modification
        // are applied to the object which has been passed, and not to a copy 
        // of it. This often leads to errors.  
        
        // We now increase the speed and then ask the bike to respect the speed limit.
        // Initially the bike has speed 100 and gear 5.
        Bicycle fastBike = new Bicycle(100,5);
        fastBike.print();
        // Now we ask the bike to respect a speed limit of 80 
        fastBike.respectSpeedLimit(80);
        fastBike.print();
        
        
        // SOME EXPERIMENTS WITH THE DIET PROBLEM 
        // First we create the data that then we pass to 
        // the constructor of the Diet Problem
        int nFoods = 5;
        int nNutrients = 3;
        // Notice this alternative way of populating an array
        double costs[] = {8,10,3,20,15};
        String foodNames[] = {"Apples","Bananas","Carrots","Dates","Eggs"};
        double nutrientsRequirement[] = {70,50,12};
        String nutrientNames[] = {"Protein","Vitamin","Iron"};
        
        double foodsComposition[][] = new double[nFoods][nNutrients];
        foodsComposition[0][0] = 0.4;
        foodsComposition[0][1] = 6;
        foodsComposition[0][2] = 0.4;
        
        foodsComposition[1][0] = 12;
        foodsComposition[1][1] = 10;
        foodsComposition[1][2] = 0.6;
        
        foodsComposition[2][0] = 6;
        foodsComposition[2][1] = 3;
        foodsComposition[2][2] = 0.4;
        
        foodsComposition[3][0] = 6;
        foodsComposition[3][1] = 1;
        foodsComposition[3][2] = 0.2;
        
        foodsComposition[4][0] = 12.2;
        foodsComposition[4][1] = 0;
        foodsComposition[4][2] = 2.6;
        
        DietProblem dp = new DietProblem(nFoods, nNutrients, costs, foodsComposition, 
                nutrientsRequirement, foodNames, nutrientNames);
        dp.print();
        
        System.out.println("The content of Vitamin in Carrots is "+dp.getFoodsComposition(3, 2));
        
        // Experiments with the optimization model
        DietProblemModel dpm = new DietProblemModel(dp);
        dpm.solve();
        dpm.printSolution();
        
        
        // EXPERIMENTS WITH THE READER AND PRINTER CLASSES
        // Notice that since the method read was declared as a static method,
        // it can be used without creating an object of the classs
        // Reader. I can simply by accessed as className.methodName()
        
        // Note: if you give only the name of the file, the file must be in the 
        // NetBeans project directory. Otherwise you must specify a full path 
        // to the file
        Reader.read("fileToRead.txt");
        
        // In the writer class I did not create a static method
        // (arbitrary choice). Therefore, in order to print, 
        // we need to create a Writer object
        Writer writer = new Writer("output.txt");
        writer.write("This line will be written in the output file");
        writer.write("Also this line will be written in the output file");
        // The output file will be created in the NetBeans project folder
        // unless you specify a different path.
        // We close the connection
        writer.close();
        
        // EXPRERIMENTS WITH THE CLASS GEOMETRY
        // A point representing the origin
        Point o1 = new Point();
        // Another point representing the origin
        Point o2 = new Point(0,0);
        o1.print();
        o2.print();
        // Let us move point o2
        o2.setX(3.5);
        o2.print();
        // Two arbitrary points
        Point p1 = new Point(11,4.5);
        Point p2 = new Point(6,9.2);
        p1.print();
        p2.print();
        
        // An empty rectangle in the origin
        Rectangle r1 = new Rectangle();
        r1.print();
        System.out.println("Is r1 empty? "+r1.isEmpty());
        // A rectangle with given dimensions in the origin
        Rectangle r2 = new Rectangle(100,120);
        r2.print();
        System.out.println("Is r2 empty? "+r2.isEmpty());
        // A rectangle in p2 with given dimensions
        Rectangle r3 = new Rectangle(300,220,p2);
        r3.print();
        System.out.println("Is r3 empty? "+r3.isEmpty());
        // Let us scale r3
        r3.scale(10);
        r3.print();
        // Is r2 bigger than r3?
        System.out.println("Is r2 bigger than r3? "+r3.hasBiggerArea(r2));
        // Is r3 bigger than r3?
        System.out.println("Is r3 bigger than r2? "+r2.hasBiggerArea(r3));
    }
    
    
}
