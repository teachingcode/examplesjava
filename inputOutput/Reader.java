package inputOutput;

// Note that in order to use the classes File and Scanner
// we need to import them from the JDK library. File is in the
// "java.io" package while Scanner is in the "java.util" package.
// Here you find all the packages in the JDK and the classes they contain. 
// https://docs.oracle.com/javase/8/docs/api/index.html 
import java.io.File;
import java.util.Scanner;
import java.io.FileNotFoundException;
 

/**
 * An example class with a method for reading a certain text file.
 * @author lct495
 */
public class Reader {
    /**
     * Reads the specified file. 
     * This is an example of a static method. Similarly to 
     * static variables, a static method belongs to the class
     * and is thus common for all instances. The method is 
     * public and thus is visible outside the class. 
     * The method receives a text file and prints its content.
     * Clearly, the code necessary for reading the file must be 
     * adapted to the specific content of the file. 
     * Note that the choice of making this method static is
     * an arbitrary choice in order to show how a static method work.
     * I could have well made it non-static. See the main file
     * for how to interact with a static method.
     * Note that this is not the only way to read a file.
     * This is just a possibly way and you may want to explore more 
     * options for different situations. 
     * @param fileName the path of the file to read
     * @throws FileNotFoundException 
     */
    public static void read(String fileName) throws FileNotFoundException {
        
        // In order to read we create an object of the class File.
        // The class file is provided by the JDK and can be imported
        // as import "java.io.File" (it is in the package java.io).
        // An object of the class File represents file objects in our computer
        // specified by their path. 
        File file = new File(fileName);

        // We create a Scanner object, an object which is able to scan a file.
        // The scanner constructor takes as input an object of the class File.
        // See the javadocs https://docs.oracle.com/javase/8/docs/api/index.html
        // for more information on its constructors and method. 
        // Some of the methods will be used in what follows.        
        Scanner scanner = new Scanner(file);
        // Notice that as soon as you create a scanner object,
        // NetBeans will tell you it can cause an exception.
        // That is, if you specify the name of a file which does not
        // exist, that will be an unplanned situation, and we need 
        // to deal with it somehow. A lot of things can be done in this case.
        // However, our strategy here is to "throw" the exception,
        // stating it in the method signature where we say
        // "throws FileNotFoundException". Since we will not
        // write any code to "catch" the exception, if the exception
        // happens it will make the application crash. Note, however, that
        // this is a perfectly sound strategy in certain situations, 
        // especially at a development phase, where we want the application
        // to stop if something weird happens so that we can fix the mistake. 
        // You can read more about exceptions here 
        // https://docs.oracle.com/javase/tutorial/essential/exceptions/index.html 
        
        // Before looking at this code have a look at the file
        // "fileToRead.txt". Given the structure of the file we 
        // tell scanner what to look for.
        
        // Returns true if there is another element to read. 
        //Does not advance past the element
        boolean hasAnotherElement = scanner.hasNext();
        if(hasAnotherElement){
            // Returns true if the next element can be interpreted as an integer. 
            // Does not advance past the element
            boolean hasNextInteger = scanner.hasNextInt();	
            if(hasNextInteger){
                    int m = scanner.nextInt();		
                    System.out.println("Integer "+m);
            }else{
                    System.out.println("The next element is not an integer ");
            }
        }
        // Checks again if there are more elements
        hasAnotherElement = scanner.hasNext();
        if(hasAnotherElement){
            // Returns true if the next element can be interpreted as a double. 
            // Does not advance past the element
            boolean hasNextDouble = scanner.hasNextDouble();	
            if(hasNextDouble){
                    double n = scanner.nextDouble();		
                    System.out.println("Double "+n);
            }else{
                    System.out.println("The next element is not a double ");
            }
        }

        // Method .nextLine() reads the remaining part of the current line 
        // and advances to the beginning of the following line. 
        System.out.println(scanner.nextLine());
        
        // Method .next() is typically used for String. 
        // It reads the next element (no matter if it is a number of anything else) as a String
        System.out.println(scanner.next());

        // Finally we need to close the connection with the file we are reading
        scanner.close();
		
    }
}
