package inputOutput;

// Note that in order to use the classes File and PrintWriter
// we need to import them from the JDK library. Both classes are in the "java.io" 
// package. Here you find all the packages in the JDK and the classes they contain. 
// https://docs.oracle.com/javase/8/docs/api/index.html 
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

/**
 * An example class for writing to a file.
 * This class provides you an example of how 
 * it is possible to write output to a file.
 * Note that alternative ways of writing to files
 * exists and may be preferable in other situation. 
 * @author lct495
 */
public class Writer {
    
    // I arbitrarily create a writer
    // class dedicated to a specific file. 
    // This is only for illustrative purposes,
    // this approach is by no means to be considered
    // suitable in all circumstances. 
    private final File file;
    private final PrintWriter writer;
    
    public Writer(String fileName) throws FileNotFoundException{
        this.file = new File(fileName);
        // We create a PrintWriter object, capable of writing to a file
        // Search for the documentation here https://docs.oracle.com/javase/8/docs/api/index.html
        this.writer = new PrintWriter(file);
        // Notice that the creation of a PrintWriter object
        // can raise an exception if the file does not exist.
        // We throw the exception as explained in other files. 
        
    }
    /**
     * A simple method which prints a string to a file.
     * @param stringToWrite
     * @throws FileNotFoundException 
     */
    public void write(String stringToWrite) throws FileNotFoundException {
        // Prints the given string and sets the cursor to the next line
        writer.println(stringToWrite);
        
    }
    /**
     * Closes the connection to the file.
     * This is an efficiency measure which allows the writer
     * to release the resources occupied (the file and the connection).
     */
    public void close(){
        writer.close();
    }
}
    

