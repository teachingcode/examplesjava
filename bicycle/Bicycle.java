package bicycle;

/**
 * This class provides the template for objects representing bicycles
 * @author Giovanni Pantuso
 */
public class Bicycle {

    // The fields of the class represent
    // the states of a bicycle
    
    // The states are private, only accessible within the class.
    // If you change the keyword "private" to "public"
    // you will be able to access the fiels without 
    // using a method, that is: nameOfTheObject.speed.
    // We decide however to make the field private.
    // This is called "encapsulation".
    private int speed;
    private int gear;
    // An example of static variable, 
    // common to all objects of the class Bicycle.
    private static int nWheels = 2; 
    
    /**
     * Provides a no-argument constructor. 
     * Replaces the default constructor (the one without arguments)
     * with another constructor which sets the speed to 0 and the gear to 1
     */
    public Bicycle(){
        speed = 0;
        gear = 1;
    }
    
    /**
     * Provides a more elaborated constructor.
     * It creates a bike with given speed and gear
     * @param speed
     * @param gear 
     */
    public Bicycle(int speed, int gear){
        // Notice that I arbitrarily named the argument
        // variables "speed" and "gear", the same as the
        // field variables. To avoid confusion, we 
        // use "this.speed". "this." refers to the field 
        // variables of the individual object. 
        // "this" can be used anywhere in the class, 
        // That is both in the constructor and in the methods.
        this.speed = speed;
        this.gear = gear;
    }
        
    /**
     * Changes the speed to a new value.
     * @param newValue 
     */
    public void changeGear(int newValue) {
        // We change the gear only if the new gear value is strictly positive.
        // Otherwise, gear remains unchanged.
        if(newValue > 0){
            gear = newValue;
        }
    }
    /**
     * Changes the speed to a new value
     * @param newValue 
     */
    public void changeSpeed(int newValue) {
        // We change the speed only if the new gear value is strictly positive.
        // Otherwise, speed remains unchanged.
        if(newValue > 0){
            speed = newValue;
        }
    }
    /**
     * Returns the current value of gear.
     * @return 
     */
    public int getGear(){
        return gear;
    }
    /**
     * Changes the static number of wheels.
     * @param newValue 
     */
    public void changeWeels(int newValue) {
        // We change nWheels only if the new gear value is strictly positive.
        // Otherwise, nWheels remains unchanged.
        if(newValue > 0){
            nWheels = newValue;
        }
    }
    /**
     * The method decreases the speed until bike respects the speed limit.
     * @param speedLimit 
     */
    public void respectSpeedLimit(int speedLimit){
        // This method illustrates the use of the while loop.
        while(speed > speedLimit){
            speed--;
        }
    }
    /**
     * The method decreases the speed until bike respects the speed limit.
     * This has the same effect of the previous method but is more efficient.
     * @param speedLimit 
     */
    public void respectSpeedLimitAlternative(int speedLimit){
        // This method illustrates the use of the method Min of the Math class.
        // The Math class has many useful methods. It is part of the 
        // "lang" package of the JDK. The "lang" package does not need to be imported 
        // contrary to other packages of the JDK. It is like a set of classes already available
        // in any application we will build. I suggest you explore other methods of the 
        // Math class, to see how they work.
        speed = Math.min(speed, speedLimit);
    }
    /**
     * Prints some information about the bike object.
     */
    public void print() {
         System.out.println(" speed:" + speed 
                 + " gear:" + gear
                 + " wheels: " + nWheels
         );
    }
    
   
}
